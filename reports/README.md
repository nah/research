## User Research Reports

We are committed to open design, so you can see the user research we have conducted in the past. 

If you want to run user research with us, read our [Guidelines to becoming a user researcher with Tor](https://community.torproject.org/user-research/guidelines/).
