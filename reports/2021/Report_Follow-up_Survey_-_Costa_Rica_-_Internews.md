# Report: Follow-up Survey - Costa Rica - Internews

The goal of this [short survey](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/scripts%20and%20activities/2021/Script_Follow-up_Survey_-_Costa_Rica_-_Internews.md) was to follow-up participants of the previous study and understand how have they been using Tor Browser in their daily-life. The previous activity consisted first of an exploratory study and then an overall presentation of TB and Internet Privacy. Thus, I wanted to get a glance of how has been their use before we dive into interviews.

The short survey was answered by 7 of 8 participants. And it consisted of 7 questions: 4 of them were about their use of Tor Browser and the remainder 3 were to identify the participant and eventually schedule an interview.

Author: [@josernitos](https://gitlab.torproject.org/josernitos) - Funded by: Internews - Feedback Collection Funding Pool 2021

## Main takeaways from study

Drawing conclusions from the [previous study](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/reports/2021/UR-Tor-CostaRica.md) and this follow-up, we can be certain that most participants are not heavy-technical users as most of them (4 of 7) would not recommend TB either because: (i) they don't feel they completely understand the technology or (ii) they think other people would find it hard to use in their day to day. 

I think is worth expanding on this issue of not recommending TB to other people. As a thought experiment, I would imagine people normally recommend products and services even though they don't know all of its details. It may be that they fear others might bump into a lot of questions and, as a result, would probably need further guidance. I think is interesting to design an interview that could get us closer to understand two things: (i) why they feel others need guidance (as they probably needed in the first place) and (ii) what are the areas they think they need more expertise to feel comfortable enough to guide others.

### General Browser usage

*Apart from using Tor Browser.*

- 3 participants (42%) used primarily one browser (Chrome) as their regular use. 
- Whilst 4 partipants (57%) used two browsers on their day to day.
	- 2 of them used Firefox and Chrome
	- 1 used Firefox and Safari
	- 1 used Chrome and Safari

### Frequency of usage (Tor Browser)

All of them used the TB, but...

- only 1 participant (14%) used it frequently
- 2 participants (28%) used it sometimes
- 4 participants (57%) used it a few times

### Would you recommend Tor Browser?

#### NPS score: -14

*I used the [NPS metric](https://hbr.org/2003/12/the-one-number-you-need-to-grow) to bracket and understand the participants relationship with the Browser. [Not a single metric is perfect](https://hbr.org/2019/10/where-net-promoter-score-goes-wrong), but I felt it was interesting to use because people are [used to answer](https://fortune.com/longform/net-promoter-score-fortune-500-customer-satisfaction-metric/) in these terms and also we can leverage the metrics categories and compare them with the actual reasons of the participant's score.* 

Using the NPS metric categories, there were:

- 3 Detractors (3, 5, 6) 
- 2 Passives (7, 7)
- 2 Promotors (9, 10)

These [resulted](https://www.surveymonkey.com/mp/net-promoter-score-calculation/) in a -14 NPS metric. But if we compare it with the actual reasons why they assigned that score, we see a more interesting picture...

#### Reasons why

**Would NOT recommend: 4**

- 2 of them said they'd like to understand better and get more used to in order to recommend it. They said that they haven't figured out completely how it works.
- 1 said it has restrictions that makes it less attractive.
- 1 said it's not an everyday use browser so they would not recommend it.

**Depends: 1**

- 1 said that it's a great tool but it could be frustrating for users who lack technical skills and thus it depends on who'd they recommend it.

**Would recommend: 2**

- 2 said that they'd recommend it because the security it provides to people. 

## Quotes

- I understand when would be a good idea to use it, but is not for everyday use.
- In order to recommend TB, I need to know a lot more to explain it to someone who knows nothing about it. I'm not an expert, thus I can't recommend it. I still struggle to understand it. I feel others prefer something more practical. Not something that needs a tutorial to understand it.
- It's a really good tool but not for everyday use. Is a bit slower than conventional browsers. Some websites don't load correctly. And for all of these reasons non-technical users might find it frustrating.
- It's been difficult transitioning from the traditional and commercial browsers. I think I need to get more comfortable using it to be able to recommend it.
- I love it's security.
- I think is an excellent option to browse safely.