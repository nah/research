## User Research Scripts and Activities

Hello!

These are our current needs for testing Tor products, as well as methodologies and testing scripts. Before running Tor user research, be sure you read our [Guidelines to becoming a user researcher with Tor](https://community.torproject.org/user-research/guidelines/).
