We practice human-centered design on building tools for internet freedom while we build community. Our user research is funded by the premises of consent, respect, and empathy. Make an impact in your local community by listening to our users. Learn how we do it and help Tor with user research.

## Be a Beta Tester
We regularly release Tor Browser Alpha versions to allow users to test software improvements and new ideas. [Sign up to be in our testing pool >](https://lists.torproject.org/cgi-bin/mailman/listinfo/ux)

## Open User Research
We put our users in the center of our development process. That is how we bring privacy-enhancing technology to the ones who more need it. Explore what we are working on and start to run user research with your local community. [See open user research >](https://community.torproject.org/user-research/open)

## Become a Community User Researcher for Tor services 
Are you a design researcher, user researcher, student, or someone interested in learning more about Tor users? Do you have new ideas, suggestions, or research that can help improve Tor applications? Help us by coordinating user research with your local community, and learn more about best practices for working with users at-risk. [Subscribe to the UX team mailing list >](https://lists.torproject.org/cgi-bin/mailman/listinfo/ux)