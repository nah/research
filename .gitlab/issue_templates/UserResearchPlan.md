
# User Research Plan

<!-- Before start, read our User Research Guidelines https://community.torproject.org/user-research/guidelines/
This and other comments should be removed as you write the plan. -->

## Goals

<!-- The research goal question should be focused, narrow, specific, answerable, feasible and open, e.g "what", "why" and "how" -->

## Audience

<!-- How many people will take part in your user research session? Which profile they are? e.g Regular Tor users, New users, Advanced users -->

### Recruitment

<!-- What characteristics should your research participants have in order to answer your research questions? Where and how will you find your participants? How will you compensate them for their time and feedback? -->

## Previous Research

<!-- Does exist previous user research on this topic? -->

## Methodology

<!-- Explain whether the tests will be co-located or remote, moderated or unmoderated, and who will attend the sessions (e.g a moderator and a note-taker) -->

## Activities

<!--For each activity, provide a brief description of what the task is about, a suggested time to do it, materials needed, and the objective of each task -->

### Description

### Suggested Time

### Materials Needed

### Outline and timing

#### 1.Introduction to the session

<!-- Here you can write the script to: Welcome participant, explain the activity, the technical setup, get consent, etc... -->

#### 2. Activity

<!-- Here you can write the tasks -->

#### 3. Debriefing

<!-- Here you can write the script for the closing interview and list any other short activities you want to run during the final minutes, e.g user satisfaction questionnaire, collect general feedback from participant, etc... -->

## Report Findings

  <!-- List the data you will collect during the study, e.g screen + audio recordings, task success rates, etc... as well as how you will present and share findings, e.g report, wiki page, presentation, etc...) -->


## Attachments

<!-- List the documents you will need to produce and bring to the user research sessions, e.g consent forms, usability testing script, questionnaires, etc... -->
